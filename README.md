# Тест SVD-разложения

Программа написана [Толстухой А.С.](http://www.univer.omsk.su/departs/math/cpivm/staff/tolstuha/) на C++ для студентов 1 курса магистратуры [ИМИТ ОмГУ](http://www.omsu.ru/about/structure/study/im/index.php). Распространяется свободно.

Изначально программа была Visual C++ проектом, я адаптировал под среду [CMake](https://cmake.org/) для кросс-платформенного использования.

В среде [CLion](https://www.jetbrains.com/clion/) ищите выходные файлы в папке сборки, т.е. в `cmake-build-debug`.

Без [CLion](https://www.jetbrains.com/clion/) проект можно собрать так (понадобится установленный системный [CMake](https://cmake.org/download)):

```
mkdir cmake-build-debug
cd cmake-build-debug
cmake ..
cmake --build .
```

Кроме того, можно регулировать тип проекта, который CMake сгенерирует для вас на выходе. По умолчанию в CLion установлен тип `CodeBlocks - Unix Makefiles`. Поменять тип проекта вы можете через [параметр `-G` команды `cmake`](https://cmake.org/cmake/help/v3.12/manual/cmake.1.html).

Подробнее о CMake-генераторах читайте в [man 7 cmake-generators](https://cmake.org/cmake/help/v3.12/manual/cmake-generators.7.html).


## Координаты точек на правом циферблате

| Время на циферблате |   x   |   y   |
| ------------------- | ----- | ----- |
| 2  ч.               | 1045  | 381   |
| 3  ч.               | 1069  | 476   |
| 6  ч.               | 949   | 626   |
| 8  ч.               | 827   | 503   |
| 9  ч.               | 806   | 406   |
| 10 ч.               | 818   | 319   |
| 12 ч.               | 929   | 259   |
| Центр               | 949   | 438   |