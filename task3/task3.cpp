﻿#include <cmath>
#include <iostream> 
#include <fstream>

using namespace std;

#define FILE_IN "d1.txt"
#define FILE_OUT "outf.txt"

ofstream logfile(FILE_OUT); // файл для отладки

int main(int argc, char **argv) {

  ifstream datafile(FILE_IN);
  logfile << FILE_IN << endl << endl;

  char s[80];
  datafile >> s;

  int pxl;
  datafile >> pxl >> s;
  logfile << "pxl=" << pxl << endl;

  int pxr;
  datafile >> pxr >> s;
  logfile << "pxr=" << pxr << endl;

  int ph;
  datafile >> ph >> s;
  logfile << "ph=" << ph << endl;

  int pp;
  datafile >> pp >> s;
  logfile << "pp=" << pp << endl;

  int xx = (pxl + pxr)*pp;
  logfile << "Количество полос по х (xx) - " << xx << endl;

  int xy = (ph*pp);
  logfile << "Количество полос по y (xy) - " << xy  << endl; 

  int n = (xx+1)*(xy+1);
  logfile << "Всего узлов (n) - " << n << endl; 


  // задать координаты

  double *x = new double[n];
  double *y = new double[n];

  logfile << endl << " x:   y: " << endl << endl;

  int i, j, k;
  int count = 0;
  double step = 1. / pp;

  double xc = -(double)pxl;

  for (i = 0; i <= xx; i++) {
    for (j = 0; j <= xy; j++) {
      x[count] = xc;
      y[count] = step*j;
      logfile << count << " " << x[count] << " " << y[count] << endl;
      count++;
    }
    xc += step;
    logfile << endl;
  }

  // массив связей
  int le = xx * xy * 2;
  int lntp = le * 3;
  int *ntp = new int[lntp];

  int ne = 0;
  ntp[0] = 0;  ntp[1] = xy + 1;  ntp[2] = 1;  ne++;
  ntp[3 * ne + 0] = 1;  ntp[3 * ne + 1] = xy + 1;  ntp[3 * ne + 2] = xy + 2;  ne++;

  int ntp_shift = 6;
  for (j = 1; j < xy; j++) {
    for (k = 0; k < ntp_shift; k++) {
      ntp[3 * ne + k] = ntp[3 * (ne - 2) + k] + 1;
    }
    ne += 2;
  }

  ntp_shift = xy * 2 * 3;
  for (j = 1; j < xx; j++) {
    for (k = 0; k < ntp_shift; k++) {
      ntp[3 * ne + k] = ntp[3 * (ne - xy * 2) + k] + xy + 1;
    }
    ne += xy * 2;
  }

  logfile << " le " << le << " ne " << ne << endl;
  logfile << endl << " ntp: " << endl;

  ne = 0;
  for (i = 0; i < xx; i++) {
    for (j = 0; j < xy; j++) {
      logfile << ne << "| " << ntp[3 * ne] << " " << ntp[3 * ne + 1] << " " << ntp[3 * ne + 2] << endl;
      ne++;

      logfile << ne << "| " << ntp[3 * ne] << " " << ntp[3 * ne + 1] << " " << ntp[3 * ne + 2] << endl;
      ne++;
    }
  }

  int lzero = (pxl*pp + 1)*(pp + 1) + pxr * pp;
  logfile << endl << "узлов с нулевым условием (lzero)" << lzero << endl;

  int lunit = xx + 1;
  logfile << endl << "узлов с условием psi =1(lunit)" << lunit << endl;

  int ldelete = lzero + lunit;
  logfile << endl << "столько строк и столбцов не будет в матрице  " << ldelete << endl;

  int *ind = new int[n];

  int czero = 0;
  int cunit = lzero;
  int cother = ldelete; // курсоры для учёта граничных условий

  int current = 0; // Текущий номер узла 

  // обрабатываем левые блоки 
  int lrows = pxl * pp + 1; // ряды узлов со ступенькой
  int under = pp + 1; // кол-во узлов в ряду под ступенькой

  for (j = 0; j < lrows; j++) {
    for (int k = 0; k < under; k++) {
      ind [current] = czero; czero++;  current++;
    }
    for (int k = under; k < xy; k++) {
      ind[current] = cother; cother++ ; current++;
    }

    ind[current] = cunit;
    cunit++;
    current++;
  }

  for (j = lrows; j < xx+1; j++) {
    ind[current] = czero; czero++;  current++;
    for (int k = 1; k < xy; k++) {
      ind[current] = cother; cother++; current++;
    }

    ind[current] = cunit; cunit++; current++;
  }

  logfile << endl << "ind:"<< endl;
  for (int  j=0; j< xx +1; j++) {
    for (int k = 0; k < xy + 1; k++) logfile << " " << ind[j*(xy + 1) + k];
    logfile << endl;
  }

  // логическая структура матрицы
  int size = n - ldelete;
  int *lh1 = new int[size];
  for (i = 0; i < size; i++) lh1[i]=1;

  int **lh = new int *[size];
  for (i = 0; i < size; i++) {
    lh[i] = new int[7];
  }
  for (current = 0, i = 0; i < n; i++) {
    if (ind[i] < ldelete) continue;
    lh[current][0] = ind[i] - ldelete;
    current++;
  }

  int l, n1, n2, num1, num2, flag;
  for (l = 0; l < le; l++) {
    for (n1 = 0; n1 < 2; n1++) {
      num1 = ind[ntp[l * 3 + n1]] - ldelete;
      if (num1 < 0) continue;
      for (n2 = n1 + 1; n2 < 3; n2++) {
        num2 = ind[ntp[l*3+n2]]-ldelete;
        if (num2 < 0) continue;
        flag = 1;
        for (j = 1; j < lh1[num1]; j++) {
          if (lh[num1][j] == num2) {
            flag = 0;
            break;
          }
        }
        if (flag) lh[num1][lh1[num1]++] = num2;
        flag = 1;
        for (j = 1; j < lh1[num2]; j++) {
          if (lh[num2][j] == num1) {
            flag = 0;
            break;
          }
        }
        if (flag) lh[num2][lh1[num2]++] = num1;
      }
    }
  }
  logfile << endl << "lh1 lh: " << endl;
  for (i = 0; i < size; i++) {
    logfile << i << " | " << lh1[i] << " | ";
    for (j = 0; j < lh1[i]; j++) logfile << " " << lh[i][j];
    logfile << endl;
  }


  for (i = 0; i < size; i++) {
    delete[] lh[i];
  }
  delete[] lh;

  delete[] lh1;

  delete[] ind;
  delete[] ntp;

  delete[] y;
  delete[] x;

  return 0;
}
