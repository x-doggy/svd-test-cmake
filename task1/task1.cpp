#include <iostream>
#include <fstream>
#include <cmath>

using namespace std;

/*//*/

/*
double matr_inf_norm ( double **a, int n );
int colib_mat_SVD ( double *c, double *xp,  double *yp, double *zp,
						double *u, double *v,
						int k_p, int *ind );
void near_triangle ( double **a, double **q, int n);
double dia_max ( double **a, int n );
void print_a ( double **a, int n );
*/

double inf_norm(double *v, int n);

double dia_max(double **a, int m, int n);

int my_SVD(double **a, int m, int n, double **q, double eps);

void SVD_Q_step_eps(double **a, double **q, int m, int n, double eps);

void print_a(const char *s, double **a, int m, int n);

void line_conic_koefs(double *C, double *le, double *ri);

/*//*/

#define M 5
#define N 6
#define EPS 1.e-21

#define FILE_OUT "conic.out"


ofstream logfile; //файл для отладки



int main(int argc, char **argv) {

    int m = M;
    int n = N;

    logfile.open(FILE_OUT);

    //коника
    //				c1 c4 c5
    // C^*_inf =	.. c2 c6
    //				.. .. c3
    double **C_inf = new double *[m];
    for (int i = 0; i < m; i++) C_inf[i] = new double[n];

    //прямые

    // l_inf: y=2
    double *l_inf = new double[3];
    l_inf[0] = 0.;
    l_inf[1] = 1.;
    l_inf[2] = -2.;
    // l_1p: -2x+3y = 0
    double *l_1p = new double[3];
    l_1p[0] = -2.;
    l_1p[1] = 3.;
    l_1p[2] = 0.;
    // l_1m: 2x+3y = 0
    double *l_1m = new double[3];
    l_1m[0] = 2.;
    l_1m[1] = 3.;
    l_1m[2] = 0.;
    // l_2p: -2x+y +4 = 0
    double *l_2p = new double[3];
    l_2p[0] = -2.;
    l_2p[1] = 1.;
    l_2p[2] = 4.;
    // l_2m: 2x+y +4 = 0
    double *l_2m = new double[3];
    l_2m[0] = 2.;
    l_2m[1] = 1.;
    l_2m[2] = 4.;

    //заполнить разрешающую матрицу
    line_conic_koefs(C_inf[0], l_inf, l_inf);

    line_conic_koefs(C_inf[1], l_1m, l_1p);
    line_conic_koefs(C_inf[2], l_1m, l_2p);
    line_conic_koefs(C_inf[3], l_2m, l_2p);
    line_conic_koefs(C_inf[4], l_2m, l_1p);

    print_a(" C_inf: ", C_inf, m, n);


    int i, j;

    double **q = new double *[n];
    for (i = 0; i < n; i++) q[i] = new double[n];
/*	for( i=0; i<n; i++ )
	{
		for( j=0; j<i; j++ ) q[i][j] = 0.;
		q[i][i] = 1.;
		for( j=i+1; j<n; j++ ) q[i][j] = 0.;
	}
*/
    double **C_copy = new double *[m];
    for (int i = 0; i < m; i++) C_copy[i] = new double[n];
    for (int i = 0; i < m; i++)
        for (int j = 0; j < n; j++) C_copy[i][j] = C_inf[i][j];


    double eps = EPS;

    int code = my_SVD(C_inf, m, n, q, eps);
    cout << " mySVD code = " << code << endl;

    print_a(" C_inf приведена к диагонали ", C_inf, m, n);

    //с.в., отвечаюший минимальному с.ч.
    double l_min = fabs(C_inf[0][0]);
    int k_lm = 0;
    for (i = 1; (i < n) && (i < m); i++) {
        if (fabs(C_inf[i][i]) < l_min) {
            l_min = fabs(C_inf[i][i]);
            k_lm = i;
        }
    }

    logfile << "l_min = " << C_inf[k_lm][k_lm] << " k_lm = " << k_lm << endl;

    double *c = new double[n];
    logfile << " egen vector: " << endl;
    for (i = 0; i < n; i++) {
        c[i] = q[i][k_lm];
        logfile << i << "  " << c[i] << endl;
    }

    //невязки исходной матрицы
    double *ra = new double[m];

    double s;
    for (i = 0; i < m; i++) {
        for (s = 0., j = 0; j < n; j++) s += C_copy[i][j] * q[j][k_lm];
        ra[i] = s;
    }
    logfile << " ra: " << endl;
    for (i = 0; i < m; i++) logfile << " " << ra[i];
    logfile << endl;
    double neviaz = inf_norm(ra, m);
    logfile << " ||r_a|| = " << neviaz << endl;

    logfile.close();

    delete[] ra;
    delete[] c;
    for (int k = 0; k < m; k++) delete[] C_copy[k];
    delete[] C_copy;
    for (int k = 0; k < n; k++) delete[] q[k];
    delete[] q;
    delete[] l_2m;
    delete[] l_2p;
    delete[] l_1m;
    delete[] l_1p;
    delete[] l_inf;
    for (int k = 0; k < m; k++) delete[] C_inf[k];
    delete[] C_inf;


    return 0;
}

void line_conic_koefs(double *C, double *le, double *ri) {
    C[0] = le[0] * ri[0];
    C[1] = le[1] * ri[1];
    C[2] = le[2] * ri[2];
    C[3] = le[0] * ri[1] + le[1] * ri[0];
    C[4] = le[0] * ri[2] + le[2] * ri[0];
    C[5] = le[1] * ri[2] + le[2] * ri[1];
}


double dia_max(double **a, int n) {
    double max = fabs(a[0][0]);
    for (int i = 1; i < n; i++) {
        if (fabs(a[i][i]) > max) {
            max = fabs(a[i][i]);
        }
    }

    return max;
}

double dia_max(double **a, int m, int n) {
    double max = fabs(a[0][0]);
    for (int i = 1; (i < n) && (i < m); i++) {
        if (fabs(a[i][i]) > max) {
            max = fabs(a[i][i]);
        }
    }

    return max;
}


int my_SVD(double **a, int m, int n, double **g, double eps) // a -- m x n matrix
{
    int i, j, k;

    for (i = 0; i < n; i++) {
        for (j = 0; j < n; j++) g[i][j] = 0.;
    }
    for (i = 0; i < n; i++) g[i][i] = 1.;

    double *wl = new double[m];
    double *wr = new double[n];

    //привести к двухдиагональной
    double modsq_a_croped, mod_a, sig, mod_inv;
    double wa;
    for (k = 0; k < n; k++) {

        cout << " k=" << k;

        if (k <= n - 2) {

            //действуем слева
            for (i = k; i < m; i++) wl[i] = a[i][k];
//		for( i=k; i<m; i++ ) cout << " " << wl[i]; cout << endl;

            for (modsq_a_croped = 0., i = k + 1; i < m; i++) modsq_a_croped += wl[i] * wl[i];
            mod_a = sqrt(wl[k] * wl[k] + modsq_a_croped);
            sig = (wl[k] > 0.) ? -1. : 1.;
            wl[k] -= sig * mod_a;
            mod_inv = 1. / sqrt(wl[k] * wl[k] + modsq_a_croped);
            for (i = k; i < m; i++) wl[i] *= mod_inv;
//		for( i=k; i<m; i++ ) cout << " " << wl[i]; cout << endl;

            for (j = k; j < n; j++) {
                for (wa = 0., i = k; i < m; i++) wa += wl[i] * a[i][j];
                wa += wa;
                for (i = k; i < m; i++) a[i][j] -= wa * wl[i];
            }

        }//if

        if (k >= n - 2) continue;

        //действуем справа
        for (j = k + 1; j < n; j++) wr[j] = a[k][j];
        for (modsq_a_croped = 0., j = k + 2; j < n; j++) modsq_a_croped += wr[j] * wr[j];
        mod_a = sqrt(wr[k + 1] * wr[k + 1] + modsq_a_croped);
        sig = (wr[k + 1] > 0.) ? -1. : 1.;
        wr[k + 1] -= sig * mod_a;
        mod_inv = 1. / sqrt(wr[k + 1] * wr[k + 1] + modsq_a_croped);
        for (j = k + 1; j < n; j++) wr[j] *= mod_inv;

        for (i = k; i < m; i++) {
            for (wa = 0., j = k + 1; j < n; j++) wa += wr[j] * a[i][j];
            wa += wa;
            for (j = k + 1; j < n; j++) a[i][j] -= wa * wr[j];
        }

        for (i = 1; i < n; i++)//первая строка не меняется
        {
            for (wa = 0., j = k + 1; j < n; j++) wa += wr[j] * g[i][j];
            wa += wa;
            for (j = k + 1; j < n; j++) g[i][j] -= wa * wr[j];
        }
    }
    print_a("a", a, m, n);
    print_a("g", g, n, n);


    double feps, u_max;

    int it = 0;
    do {
        it++;

        //вращение
        SVD_Q_step_eps(a, g, m, n, eps);

        feps = eps * dia_max(a, m, n);
        for ((u_max = 0., i = 0); (i < n - 1) && (i < m); i++)
            if (fabs(a[i][i + 1]) > u_max) u_max = fabs(a[i][i + 1]);

    } while (u_max > feps);

    logfile << endl << " it: " << it << " feps = " << feps << endl;
    print_a("a", a, m, n);
    print_a("g", g, n, n);

    delete[] wr;
    delete[] wl;

    return 1;
}


void print_a(double **a, int n) {
    int i, j;

    logfile << endl << " a:" << endl;
    for (i = 0; i < n; i++) {
        for (j = 0; j < n; j++) logfile << "  " << a[i][j];
        logfile << endl;
    }
}


void print_a(const char *s, double **a, int m, int n) {
    int i, j;

    logfile << endl << s << ":" << endl;
    for (i = 0; i < m; i++) {
        for (j = 0; j < n; j++) logfile << "  " << a[i][j];
        logfile << endl;
    }
}


void SVD_Q_step_eps(double **a, double **q, int m, int n, double eps) {

    int i, j;

    //LQ итерации
    double v1, v2, v_mod, v_min, c, s, ai, aj, ak;

    double feps = eps * dia_max(a, m, n);


    // LQ-step (L = A Q^t, Q^t = Q^t_n-1,n ... Q^t_12)
    for (int k = 0; (k < n - 1) && (k < m - 1); k++) {
        v1 = a[k][k];
        v2 = a[k][k + 1];
        if (fabs(v2) < feps) continue;
        if (fabs(v1) >= fabs(v2)) {
            v_min = v2 /= fabs(v1);
            v1 = (v1 > 0.) ? 1. : -1.;
        } else {
            v_min = v1 /= fabs(v2);
            v2 = (v2 > 0.) ? 1. : -1.;
        }
        v_mod = sqrt(1. + v_min * v_min);
        c = v1 / v_mod;
        s = v2 / v_mod;

        for (i = k; i <= k + 1; i++) {
            ai = a[i][k];
            ak = a[i][k + 1];
            a[i][k] = c * ai + s * ak;
            a[i][k + 1] = -s * ai + c * ak;
        }
//		a[k][k+1] = 0.;

        for (i = 0; i < n; i++) {
            ai = q[i][k];
            ak = q[i][k + 1];
            q[i][k] = c * ai + s * ak;
            q[i][k + 1] = -s * ai + c * ak;
        }
    }


    //left
    for (int k = 0; (k < n - 1) && (k < m - 1); k++) {
        v1 = a[k][k];
        v2 = a[k + 1][k];
        if (fabs(v2) < feps) continue;
        if (fabs(v1) >= fabs(v2)) {
            v_min = v2 /= fabs(v1);
            v1 = (v1 > 0.) ? 1. : -1.;
        } else {
            v_min = v1 /= fabs(v2);
            v2 = (v2 > 0.) ? 1. : -1.;
        }
        v_mod = sqrt(1. + v_min * v_min);
        c = v1 / v_mod;
        s = -v2 / v_mod;

        for (j = k; j <= k + 1; j++) {
            ai = a[k][j];
            aj = a[k + 1][j];
            a[k][j] = c * ai - s * aj;
            a[k + 1][j] = s * ai + c * aj;
        }
//		a[m+1][m] = 0.;

    }

}


double inf_norm(double *v, int n) {
    int i;
    double max = 0.;
    for (i = 0; i < n; i++) {
        if (max < fabs(v[i])) max = fabs(v[i]);
    }

    return max;
}
