#include <iostream>
#include <fstream>
#include <cmath>
using namespace std;

#define pi 3.14159265358979323846

/*//*/
double inf_norm(double *, int);
double matr_inf_norm(double **, int);
/*
int colib_mat_SVD ( double *c, double *xp,  double *yp, double *zp,
double *u, double *v,
int k_p, int *ind );
*/
double dia_max(double **, int);
double dia_max(double **, int, int);

/*
 * void near_triangle ( double **a, double **q, int n );
 */

int my_SVD(double **, int, int, double **, double);
void SVD_Q_step_eps(double **, double **, int, int, double);
//void print_a(double **a, int n);
//void print_a(char *s, double **a, int m, int n);
void line_conic_koefs(double *, double *, double *);
/*//*/

#define M 5
#define N 9
#define EPS 1.e-21  
#define SQRT3_2 (0.5 * sqrt(3))

//10x9
#define FILE_OUT "homo1.txt"


ofstream logfile(FILE_OUT); //файл для отладки

void print_a(double **a, int n) {
  int i, j;

  logfile << endl << " a:" << endl;
  for (i = 0; i < n; i++) {
    for (j = 0; j < n; j++) logfile << "  " << a[i][j];
    logfile << endl;
  }
}

void print_a(char *s, double **a, int m, int n) {
  int i, j;

  logfile << endl << s << ":" << endl;
  for (i = 0; i < m; i++) {
    for (j = 0; j < n; j++) logfile << "  " << a[i][j];
    logfile << endl;
  }
}

void print_a(const char s[9], double **a, int m, int n) {
  int i, j;

  logfile << endl << s << ":" << endl;
  for (i = 0; i < m; i++) {
    for (j = 0; j < n; j++) logfile << "  " << a[i][j];
    logfile << endl;
  }
}

void point_homo(double *L1, double *L2, double *P_Im, double *Im) {
  L1[0] = P_Im[0];
  L1[1] = P_Im[1];
  L1[2] = P_Im[2];
  L1[3] = 0.;
  L1[4] = 0.;
  L1[5] = 0.;
  L1[6] = -P_Im[0] * Im[0];
  L1[7] = -P_Im[1] * Im[0];
  L1[8] = -P_Im[2] * Im[0];

  L2[0] = 0.;
  L2[1] = 0.;
  L2[2] = 0.;
  L2[3] = P_Im[0];
  L2[4] = P_Im[1];
  L2[5] = P_Im[2];
  L2[6] = -P_Im[0] * Im[1];
  L2[7] = -P_Im[1] * Im[1];
  L2[8] = -P_Im[2] * Im[1];
}

double inf_norm(double *v, int n) {
  int i;
  double max = 0.;
  for (i = 0; i < n; i++) {
    if (max < fabs(v[i])) max = fabs(v[i]);
  }

  return max;
}

int main(int argc, char **argv) {
  int m = M;
  int n = N;

  //точки праобраза 6 шт
  double **P_Im = new double*[m];
  for (int i = 0; i < m; i++) P_Im[i] = new double[3];

  P_Im[0][0] = SQRT3_2;
  P_Im[0][1] = .5;
  P_Im[0][2] = 1.;
  P_Im[1][0] = 0.;
  P_Im[1][1] = -1.;
  P_Im[1][2] = 1.;
  P_Im[2][0] = -SQRT3_2;
  P_Im[2][1] = -.5;
  P_Im[2][2] = 1.;
  P_Im[3][0] = -SQRT3_2;
  P_Im[3][1] = .5;
  P_Im[3][2] = 1.;
  P_Im[4][0] = -1.;
  P_Im[4][1] = 0;
  P_Im[4][2] = 1.;

  //точки образа
  double **Im = new double*[m];
  for (int i = 0; i < m; i++) Im[i] = new double[2];

  Im[0][0] = 1045.0;
  Im[0][1] = 381.0;   // 2 часа
  Im[1][0] = 949.0;
  Im[1][1] = 626.0;   // 6 часов
  Im[2][0] = 827.0;
  Im[2][1] = 503.0;   // 8 часов
  Im[3][0] = 818.0;
  Im[3][1] = 319.0;   // 10 часов
  Im[4][0] = 806.0;
  Im[4][1] = 406.0;   // 9 часов

  // Гомография вытянутая по строчкам 
  double *H = new double[n];

  // Разрешающая матрица 
  int mm = m + m;

  double **L = new double*[mm];
  for (int i = 0; i < mm; i++) L[i] = new double[n];

  for (int i = 0; i < m; i++) {
    point_homo(L[2 * i], L[2 * i + 1], P_Im[i], Im[i]);
  }

  print_a(" L: ", L, mm, n);


  int i, j;

  double **q = new double *[n];
  for (i = 0; i < n; i++) q[i] = new double[n];

  double **L_copy = new double *[mm];
  for (int i = 0; i < mm; i++) L_copy[i] = new double[n];
  for (int i = 0; i < mm; i++)
    for (int j = 0; j < n; j++) L_copy[i][j] = L[i][j];


  double eps = EPS;

  int code = my_SVD(L, mm, n, q, eps);
  cout << " mySVD code = " << code << endl;

  print_a(" L приведена к диагонали ", L, mm, n);

  //с.в., отвечаюший минимальному с.ч.
  double l_min = fabs(L[0][0]);
  int k_lm = 0;
  for (i = 1; (i < n) && (i < mm); i++) {
    if (fabs(L[i][i]) < l_min) {
      l_min = fabs(L[i][i]);
      k_lm = i;
    }
  }


  logfile << "l_min = " << L[k_lm][k_lm] << " k_lm = " << k_lm << endl;
  double *c = new double[n];
  logfile << " egen vector: " << endl;
  for (i = 0; i < n; i++) {
    c[i] = q[i][k_lm];
    logfile << i << "  " << c[i] << endl;
  }

  //невязки исходной матрицы
  double *ra = new double[mm];

  double s;
  for (i = 0; i < mm; i++) {
    for (s = 0., j = 0; j < n; j++) s += L_copy[i][j] * q[j][k_lm];
    ra[i] = s;
  }
  logfile << " ra: " << endl;
  for (i = 0; i < mm; i++) logfile << " " << ra[i];
  logfile << endl;
  double neviaz = inf_norm(ra, mm);
  logfile << " ||r_a|| = " << neviaz << endl;


  return 0;
}

void line_conic_koefs(double *C, double *le, double *ri) {
  C[0] = le[0] * ri[0];
  C[1] = le[1] * ri[1];
  C[2] = le[2] * ri[2];
  C[3] = le[0] * ri[1] + le[1] * ri[0];
  C[4] = le[0] * ri[2] + le[2] * ri[0];
  C[5] = le[1] * ri[2] + le[2] * ri[1];
}

double dia_max(double **a, int n) {
  double max = fabs(a[0][0]);
  for (int i = 1; i < n; i++) {
    if (fabs(a[i][i]) > max) {
      max = fabs(a[i][i]);
    }
  }

  return max;
}

double dia_max(double **a, int m, int n) {
  double max = fabs(a[0][0]);
  for (int i = 1; (i < n) && (i < m); i++) {
    if (fabs(a[i][i]) > max) {
      max = fabs(a[i][i]);
    }
  }

  return max;
}

int my_SVD(double **a, int m, int n, double **g, double eps) // a -- m x n matrix
{
  int i, j, k;

  for (i = 0; i < n; i++) {
    for (j = 0; j < n; j++) g[i][j] = 0.;
  }
  for (i = 0; i < n; i++) g[i][i] = 1.;

  double *wl = new double[m];
  double *wr = new double[n];

  //привести к двухдиагональной
  double modsq_a_croped, mod_a, sig, mod_inv;
  double wa;
  for (k = 0; k < n; k++) {

    if (k <= n - 2) {

      //действуем слева	
      for (i = k; i < m; i++) wl[i] = a[i][k];

      for (modsq_a_croped = 0., i = k + 1; i < m; i++) modsq_a_croped += wl[i] * wl[i];
      mod_a = sqrt(wl[k] * wl[k] + modsq_a_croped);
      sig = (wl[k] > 0.) ? -1. : 1.;
      wl[k] -= sig * mod_a;
      mod_inv = 1. / sqrt(wl[k] * wl[k] + modsq_a_croped);
      for (i = k; i < m; i++) wl[i] *= mod_inv;

      for (j = k; j < n; j++) {
        for (wa = 0., i = k; i < m; i++) wa += wl[i] * a[i][j];
        wa += wa;
        for (i = k; i < m; i++) a[i][j] -= wa * wl[i];
      }

    }//if 

    if (k >= n - 2) continue;

    //действуем справа
    for (j = k + 1; j < n; j++) wr[j] = a[k][j];
    for (modsq_a_croped = 0., j = k + 2; j < n; j++) modsq_a_croped += wr[j] * wr[j];
    mod_a = sqrt(wr[k + 1] * wr[k + 1] + modsq_a_croped);
    sig = (wr[k + 1] > 0.) ? -1. : 1.;
    wr[k + 1] -= sig * mod_a;
    mod_inv = 1. / sqrt(wr[k + 1] * wr[k + 1] + modsq_a_croped);
    for (j = k + 1; j < n; j++) wr[j] *= mod_inv;

    for (i = k; i < m; i++) {
      for (wa = 0., j = k + 1; j < n; j++) wa += wr[j] * a[i][j];
      wa += wa;
      for (j = k + 1; j < n; j++) a[i][j] -= wa * wr[j];
    }

    for (i = 1; i < n; i++)//первая строка не меняется
    {
      for (wa = 0., j = k + 1; j < n; j++) wa += wr[j] * g[i][j];
      wa += wa;
      for (j = k + 1; j < n; j++) g[i][j] -= wa * wr[j];
    }
  }
  print_a("a", a, m, n);
  print_a("g", g, n, n);


  double feps, u_max;

  int it = 0;
  do {
    it++;

    //вращение
    SVD_Q_step_eps(a, g, m, n, eps);

    feps = eps * dia_max(a, m, n);
    for ((u_max = 0., i = 0); (i < n - 1) && (i < m - 1); i++) // было i < m
      if (fabs(a[i][i + 1]) > u_max) u_max = fabs(a[i][i + 1]);

  } while (u_max > feps);

  logfile << endl << " it: " << it << " feps = " << feps << endl;
  print_a("a", a, m, n);
  print_a("g", g, n, n);

  return 1;
}

void SVD_Q_step_eps(double ** a, double **q, int m, int n, double eps) {

  int i, j;

  //LQ итерации
  double v1, v2, v_mod, v_min, c, s, ai, aj, ak;

  double feps = eps * dia_max(a, m, n);


  // LQ-step (L = A Q^t, Q^t = Q^t_n-1,n ... Q^t_12)
  for (int k = 0; (k < n - 1) && (k < m - 1); k++) {
    v1 = a[k][k];
    v2 = a[k][k + 1];
    if (fabs(v2) < feps) continue;
    if (fabs(v1) >= fabs(v2)) {
      v_min = v2 /= fabs(v1);
      v1 = (v1 > 0.) ? 1. : -1.;
    } else {
      v_min = v1 /= fabs(v2);
      v2 = (v2 > 0.) ? 1. : -1.;
    }
    v_mod = sqrt(1. + v_min * v_min);
    c = v1 / v_mod;
    s = v2 / v_mod;

    for (i = k; i <= k + 1; i++) {
      ai = a[i][k];
      ak = a[i][k + 1];
      a[i][k] = c * ai + s * ak;
      a[i][k + 1] = -s * ai + c * ak;
    }
    //		a[k][k+1] = 0.;

    for (i = 0; i < n; i++) {
      ai = q[i][k];
      ak = q[i][k + 1];
      q[i][k] = c * ai + s * ak;
      q[i][k + 1] = -s * ai + c * ak;
    }
  }


  //left
  for (int k = 0; (k < n - 1) && (k < m - 1); k++) {
    v1 = a[k][k];
    v2 = a[k + 1][k];
    if (fabs(v2) < feps) continue;
    if (fabs(v1) >= fabs(v2)) {
      v_min = v2 /= fabs(v1);
      v1 = (v1 > 0.) ? 1. : -1.;
    } else {
      v_min = v1 /= fabs(v2);
      v2 = (v2 > 0.) ? 1. : -1.;
    }
    v_mod = sqrt(1. + v_min * v_min);
    c = v1 / v_mod;
    s = -v2 / v_mod;

    for (j = k; j <= k + 1; j++) {
      ai = a[k][j];
      aj = a[k + 1][j];
      a[k][j] = c * ai - s * aj;
      a[k + 1][j] = s * ai + c * aj;
    }
    //		a[m+1][m] = 0.;

  }

}
